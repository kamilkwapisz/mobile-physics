import sqlite3

from stations.settings import STATIONS_DB_NAME


def create_table(cursor):
    creation_query = """
        CREATE TABLE IF NOT EXISTS
        stations(
            id TEXT PRIMARY KEY,
            name TEXT NOT NULL,
            latitude REAL,
            longitude REAL,
            last_pm25_measurement REAL,
            latest_measurement_datetime REAL
        )
    """
    cursor.execute(creation_query)


if __name__ == "__main__":
    db_connection = sqlite3.connect(STATIONS_DB_NAME)
    cursor = db_connection.cursor()
    create_table(cursor)
