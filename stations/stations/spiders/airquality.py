# -*- coding: utf-8 -*-
import json

import scrapy
from scrapy.exceptions import CloseSpider
from stations.extractors import jmespath_get
from stations.loaders import StationItemLoader
from stations.settings import AIRQUALITY_MONITORS_HEADERS


class AirqualitySpider(scrapy.Spider):
    # TODO: envy
    name = 'airquality'
    allowed_domains = ['airquality.ie']
    starting_url = "https://airquality.ie/assets/php/get-monitors.php"

    def start_requests(self):
        yield scrapy.Request(
            url=self.starting_url,
            callback=self.parse_stations,
            headers=AIRQUALITY_MONITORS_HEADERS,
            dont_filter=True
        )

    def parse_stations(self, response: scrapy.http.Response):
        try:
            json_data = json.loads(response.text)
        except json.decoder.JSONDecodeError:
            raise CloseSpider("Given URL does not return JSON response")
        stations = jmespath_get("[]", json_data)
        for station in stations:
            station_loader = StationItemLoader()
            station_loader.add_value(
                "name",
                jmespath_get("[label, location] | join(`, `, @)", station)
            )
            station_loader.add_value("id", jmespath_get("code", station))
            station_loader.add_value("latitude", float(jmespath_get("latitude", station)))
            station_loader.add_value("longitude", float(jmespath_get("longitude", station)))
            station_loader.add_value("last_pm25_measurement", jmespath_get("latest_reading.pm2_5", station))
            station_loader.add_value("latest_measurement_datetime", jmespath_get("latest_reading.recorded_at", station))
            yield station_loader.load_item()
