# -*- coding: utf-8 -*-
import scrapy
from stations.items import DestinationItem


class CheckmybusSpider(scrapy.Spider):
    name = 'checkmybus'
    allowed_domains = ['checkmybus.co.uk']
    starting_url = "https://www.checkmybus.co.uk/buslines"
    custom_settings = {
        'ITEM_PIPELINES': {}  # disabling pipelines
    }

    def start_requests(self):
        yield scrapy.Request(
            url=self.starting_url,
            callback=self.parse_countries,
            dont_filter=True
        )

    def parse_countries(self, response: scrapy.http.Response):
        countries_links = response.xpath(".//div[@class='countries-container']//a/@href").getall()
        for link in countries_links:
            yield scrapy.Request(
                url=response.urljoin(link),
                callback=self.parse_country_destinations
            )

    def parse_country_destinations(self, response: scrapy.http.Response):
        city_destinations_links = response.xpath(".//div[@class='origin2destinationsbyletter-container']//a/@href").getall()
        for link in city_destinations_links:
            yield scrapy.Request(
                url=response.urljoin(link),
                callback=self.parse_destination
            )

    def parse_destination(self, response: scrapy.http.Response):
        destinations = response.xpath(".//div[@class='cities-container']//a/text()").getall()
        for destination_name in destinations:
            destination = DestinationItem(name=destination_name)
            yield destination
