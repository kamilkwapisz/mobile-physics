# -*- coding: utf-8 -*-

import scrapy
from scrapy.loader.processors import TakeFirst


class StationItem(scrapy.Item):
    name = scrapy.Field()
    id = scrapy.Field()
    latitude = scrapy.Field()
    longitude = scrapy.Field()
    geolocation = scrapy.Field()
    last_pm25_measurement = scrapy.Field()
    latest_measurement_datetime = scrapy.Field()


class DestinationItem(scrapy.Item):
    name = scrapy.Field()
