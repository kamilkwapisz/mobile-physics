import re
from typing import Any

import jmespath


def jmespath_get(
    pattern: str,
    data: dict,
    regex: str = None,
    default_value: Any = None
):
    result = jmespath.search(pattern, data)
    if result is None:
        return default_value
    if regex is not None:
        regex_result = re.search(regex, result)
        if regex_result:
            return regex_result.group(1)
    return result
