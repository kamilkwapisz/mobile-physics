from scrapy import Request, Spider
from user_agent import generate_user_agent


class UserAgentMiddleware(object):
    @classmethod
    def process_request(cls, request: Request, spider: Spider):
        """
        Rotating user agent every request
        """
        request.headers['User-Agent'] = generate_user_agent(
            os=('win', )
        )
