import json

from scrapy.loader import ItemLoader
from stations.items import StationItem
from scrapy.loader.processors import TakeFirst


class StationItemLoader(ItemLoader):
    default_item_class = StationItem
    default_output_processor = TakeFirst()
