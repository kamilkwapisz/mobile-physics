import sqlite3

from stations.settings import STATIONS_DB_NAME


db_connection = sqlite3.connect(STATIONS_DB_NAME)
cursor = db_connection.cursor()


def insert_item(item: dict):
    values = item.values()
    values_placeholder = '?, ' * len(values)
    query = f"""
        INSERT INTO
        stations ({', '.join(item.keys())})
        VALUES ({values_placeholder.strip(', ')})
    """
    cursor.execute(query, tuple(values))
    db_connection.commit()
