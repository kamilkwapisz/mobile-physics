from datetime import datetime
from scrapy.exceptions import DropItem

from stations.database import insert_item
from stations.items import StationItem
from stations.settings import MEASUREMENTS_DT_FORMAT
from stations.validators import validate_coordinates


class StationSavingPipeline:
    def process_item(self, item, spider):
        insert_item(item)
        return item


class DataValidationPipeline:
    def process_item(self, item, spider):
        if isinstance(item, StationItem):
            if validate_coordinates(item.get('latitude'), item.get('longitude')) is False:
                raise DropItem(f"Coordinates for station {item.get('id')} are invalid")
        return item


class DateProcessingPipeline:
    def process_item(self, item, spider):
        try:
            dt = datetime.strptime(
                item.get('latest_measurement_datetime'),
                MEASUREMENTS_DT_FORMAT
            )
        except ValueError:
            raise DropItem(f"Latest measurement datetime {item.get('latest_measurement_datetime')} is invalid")
        except TypeError:
            return item
        else:
            item['latest_measurement_datetime'] = dt.timestamp()
            return item
