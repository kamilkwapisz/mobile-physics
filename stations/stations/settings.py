from decouple import config

BOT_NAME = 'stations'

SPIDER_MODULES = ['stations.spiders']
NEWSPIDER_MODULE = 'stations.spiders'

ROBOTSTXT_OBEY = False

# concurrency setting
CONCURRENT_REQUESTS = config("CONCURRENT_REQUESTS", default=16, cast=int)
DOWNLOAD_DELAY = config("DOWNLOAD_DELAY", default=2, cast=int)
CONCURRENT_REQUESTS_PER_DOMAIN = config("CONCURRENT_REQUESTS_PER_DOMAIN", default=8, cast=int)
CONCURRENT_REQUESTS_PER_IP = config("CONCURRENT_REQUESTS_PER_IP", default=8, cast=int)
AUTOTHROTTLE_ENABLED = config("AUTOTHROTTLE_ENABLED", default=True, cast=bool)
AUTOTHROTTLE_START_DELAY = config("AUTOTHROTTLE_START_DELAY", default=2, cast=int)
AUTOTHROTTLE_TARGET_CONCURRENCY = config("AUTOTHROTTLE_TARGET_CONCURRENCY", default=8, cast=int)

# Enable or disable downloader middlewares
DOWNLOADER_MIDDLEWARES = {
    'stations.middlewares.UserAgentMiddleware': 500,
}

# Configure item pipelines
ITEM_PIPELINES = {
    'stations.pipelines.DataValidationPipeline': 100,
    'stations.pipelines.DateProcessingPipeline': 200,
    'stations.pipelines.StationSavingPipeline': 800,
}

# Special headers
AIRQUALITY_MONITORS_HEADERS = {
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'Host': 'airquality.ie',
    'Referer': config("AIRQUALITY_REFERER", default="https://airquality.ie/station/EPA-54"),
}

# DB settings
STATIONS_DB_NAME = config("STATIONS_DB_NAME", default="air_stations.db")

# datetime parsing
MEASUREMENTS_DT_FORMAT = "%Y-%m-%d %H:%M:%S"
