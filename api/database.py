import sqlite3
from decouple import config

db_connection = sqlite3.connect(
    config("STATIONS_DB_NAME", default="air_stations.db")
)
db_connection.row_factory = sqlite3.Row
cursor = db_connection.cursor()


def dict_factory(cursor, row):
    data = {}
    for idx, column in enumerate(cursor.description):
        data[column[0]] = row[idx]
    return data


def read_stations() -> list:
    query = """
        SELECT name, id, latitude, longitude, last_pm25_measurement, latest_measurement_datetime
        FROM stations
    """
    cursor.execute(query)
    return [dict(row) for row in cursor.fetchall()]
