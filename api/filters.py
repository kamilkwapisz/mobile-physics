from datetime import date
from typing import List, Tuple

from geo import calculate_distance 
from parsers import parse_timestamp


def filter_stations(
    stations: List[dict],
    coordinates: Tuple[float],
    radius: float,
    input_date: date
) -> list:
    filtered_stations = list()
    for station in stations:
        measurement_dt = parse_timestamp(station.get('latest_measurement_datetime'))
        if input_date and measurement_dt and measurement_dt.date() != input_date:
            continue
        distance = calculate_distance(
            station_coordinates=(station.get('latitude'), station.get('longitude')),
            user_coordinates=coordinates,
        )
        if distance < radius:
            if measurement_dt:
                station['latest_measurement_datetime'] = measurement_dt.strftime("%Y-%m-%d %H:%M:%S")
            station['distance'] = distance
            filtered_stations.append(station)
    return filtered_stations
