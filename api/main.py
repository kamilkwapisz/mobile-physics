from datetime import date as date_format
from typing import Optional

from fastapi import FastAPI, HTTPException

from database import read_stations
from filters import filter_stations


app = FastAPI()


@app.get("/stations/")
async def stations(
    latitude: float,
    longitude: float,
    radius: float = 5.0,
    date: Optional[date_format] = None
):
    all_stations = read_stations()
    stations = filter_stations(
        stations=all_stations,
        coordinates=(latitude, longitude),
        radius=radius,
        input_date=date,
    )
    if not stations:
        raise HTTPException(status_code=404, detail=f"No stations within {radius} km found")
    return stations
