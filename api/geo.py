from math import sin, cos, sqrt, atan2, radians
from typing import Tuple


EARTH_RADIUS = 6373.0

# lat1 = radians(52.2296756)
# lon1 = radians(21.0122287)
# lat2 = radians(52.406374)
# lon2 = radians(16.9251681)

# dlon = lon2 - lon1
# dlat = lat2 - lat1

# a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
# c = 2 * atan2(sqrt(a), sqrt(1 - a))

# distance = EARTH_RADIUS * c


def calculate_distance(
    station_coordinates: Tuple[float],
    user_coordinates: Tuple[float],
) -> float:
    station_latitude = radians(station_coordinates[0])
    station_longitude = radians(station_coordinates[1])
    user_latitude = radians(user_coordinates[0])
    user_longitude = radians(user_coordinates[1])

    latitude_distance = station_latitude - user_latitude
    longitude_distance = station_longitude - user_longitude

    a = sin(latitude_distance / 2) ** 2 \
        + cos(user_latitude) * cos(station_latitude) \
        * sin(longitude_distance / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    distance = EARTH_RADIUS * c
    return distance
