from datetime import datetime
from typing import Type, Union


def parse_timestamp(timestamp: Union[float, int]) -> datetime:
    try:
        dt = datetime.fromtimestamp(timestamp)
    except (OSError, TypeError):
        return None
    else:
        return dt
