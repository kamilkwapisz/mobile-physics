# Web Scraping candidate task
### Requirements
I've developed this code using Python3.8. I decided to use Scrapy. All requirements are in **requirements.txt** file. Scrapy project takes advantage of loading enviromental variables from .env file, but no extra files needed as variables have their defaults. 

### Repo structure
Repo contains directories:

* **stations/** -- Code for tasks #1 and #2. It's scrapy project with empty database file.
* File `stations/spiders/airquality.py` -- Spider for task #2
* File `stations/spiders/checkmybus.py` -- Spider for task #1
* **api/** -- Code for task #3. It's FastAPI project and .sql file with example data inside. 

### Decisions justification
#### Task #1
During website recognition I found an API link that websites uses to populate "from" select: https://autocomplete.checkmybus.com/api/complete/en-gb/52.1417/20.7196/?count=12
Unfortunately I haven't found any pagination parameter, only the count which limits up to 20. In order to retrieve all destinations we can bruteforce querying station name and play around with coordinates. Instead of that, I scraped all the origins they report on their site.

Script can be run with command:
```
scrapy crawl checkmybus -o locations.csv
```

#### Tasks #2 and #3

Script in task #2 can be run by command:
```
scrapy crawl airquality
```

Running REST app for task #3:
```
uvicorn main:app --reload
```

##### SQL
I decided to use SQLite as it's the most portable and in case you want to run it should cause no problems and does not increase the level of project's settings complexity. If you prefer, I can rewrite it to use other SQL engine. 
#### Scrapy project settings
For this project I used no proxies, as the free proxies are usually of a low quality and can cause many issues. 


##### Choice of source to scrape stations data from
While I was doing the research on the airquality site I realised that all data can be fetched directly from their API instead of HTML source. I decided to go with this solution as it's much optimal, cause we do only 1 request. Data coming from API are also well organised. 
Website uses mostly css classes which does not represent the needed data explicitly. That's why I decided to scrape the data from API. In my opinion the API structure is less likely to change throughout the time. 